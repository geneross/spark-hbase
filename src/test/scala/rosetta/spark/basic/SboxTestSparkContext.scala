package rosetta.spark.basic

import java.io.File
import java.nio.file.{Files, Paths}

import org.apache.commons.io.FileUtils
import org.apache.spark.SparkContext
import org.apache.spark.sql.{DataFrame, SQLContext, SQLImplicits, SparkSession}
import org.scalatest.{BeforeAndAfterAll, Suite}

trait SboxTestSparkContext extends BeforeAndAfterAll { self: Suite =>
  @transient var spark: SparkSession = _
  @transient var sc: SparkContext = _
  @transient var sqlContext: SQLContext = _
  @transient var sparkMajorVersion: Int = _
  @transient var sparkMinorVersion: Int = _

  protected object testImplicits extends SQLImplicits {
    protected override def _sqlContext: SQLContext = self.sqlContext
  }

  def loadOrcToTable(orcFilename: String, tableName: String): DataFrame = {
    val df = spark.read
      .format("orc")
      .load(getClass.getResource(orcFilename).toURI.toString)
      .repartition(24)
    df.createOrReplaceTempView(tableName)
    df
  }

  def getResourcePath(rName: String): String = {
    getClass.getResource(rName).toURI.toString
  }

  override def beforeAll() {
    super.beforeAll()

    // see https://issues.apache.org/jira/browse/SPARK-22918
    System.setSecurityManager(null)

    spark = SparkSession.builder()
      .master("local[*]")
      .appName(s"SboxUnitTest-${self.suiteName}-${scala.util.Random.nextInt.toString}")
      .config("spark.default.parallelism", "1")
      .config("spark.sql.shuffle.partitions", 4)
      .config("javax.jdo.option.ConnectionURL", s"jdbc:derby:memory:metastore_db_${self.suiteName};create=true")
      .config("spark.sql.warehouse.dir", s"${Paths.get("").toAbsolutePath.toString}/spark-warehouse_${self.suiteName}")
//      .config("spark.sql.hive.metastore.version", "1.2.1")
//      .config("spark.sql.hive.metastore.jars", "builtin")
      .enableHiveSupport()
      .getOrCreate()

    val checkpointDir = Files.createTempDirectory(this.getClass.getName + "-" + scala.util.Random.nextInt.toString).toString
    spark.sparkContext.setCheckpointDir(checkpointDir)
    sc = spark.sparkContext
    sqlContext = spark.sqlContext

//    spark.sqlContext.setConf("hive.exec.dynamic.partition", "true")
//    spark.sqlContext.setConf("hive.exec.dynamic.partition.mode", "nonstrict")
  }

  override def afterAll() {
    val checkpointDir = sc.getCheckpointDir
    if (spark != null) {
      val warehouseDir = spark.conf.get("spark.sql.warehouse.dir")
      spark.stop()
      FileUtils.deleteDirectory(new File(warehouseDir))
    }
    spark = null
    sqlContext = null
    sc = null

    checkpointDir.foreach { dir =>
      FileUtils.deleteQuietly(new File(dir))
    }
    super.afterAll()
  }
}
