package rosetta.spark.basic.test

import rosetta.spark.basic.{SboxTestSparkContext, SparkFunSuite}

class ExampleTest extends SparkFunSuite with SboxTestSparkContext {

  test("Загрузка данных") {
    spark.sql("drop table if exists salaries")
    spark.sql("create table if not exists salaries(salary string)")// location '/tmp/default'")
    spark.sql("insert into table salaries values ('1'), ('2')")
    spark.table("salaries").show()
  }
}
