package rosetta.spark.basic

import com.typesafe.scalalogging.LazyLogging
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{FunSuite, Outcome}
import org.slf4j.Logger


@RunWith(classOf[JUnitRunner])
abstract class SparkFunSuite extends FunSuite with LazyLogging {

  @transient protected lazy val sparkLogger: Logger = logger.underlying

  final protected override def withFixture(test: NoArgTest): Outcome = {
    val testName = test.text
    val suiteName = this.getClass.getName
    try {
      logInfo(s"\n\n===== TEST OUTPUT FOR $suiteName: '$testName' =====\n")
      test()
    } finally {
      logInfo(s"\n\n===== FINISHED $suiteName: '$testName' =====\n")
    }
  }

  protected def logDebug(s: => String): Unit = {
    if (sparkLogger.isDebugEnabled) logger.debug(s)
  }

  protected def logWarn(s: => String): Unit = {
    if (sparkLogger.isWarnEnabled) logger.warn(s)
  }

  protected def logInfo(s: => String): Unit = {
    if (sparkLogger.isInfoEnabled) logger.info(s)
  }

  protected def logTrace(s: => String): Unit = {
    if (sparkLogger.isTraceEnabled) logger.trace(s)
  }
}
