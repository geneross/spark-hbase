package rosetta.spark.basic

import java.security.PrivilegedExceptionAction

import org.apache.hadoop.hbase.client.Put
import org.apache.spark.{SparkConf, TaskContext}
import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.hbase.{HBaseConfiguration, TableName}
import org.apache.hadoop.hbase.client.{Connection, ConnectionFactory}
import org.apache.hadoop.hbase.util.Bytes
import org.apache.hadoop.security.UserGroupInformation
import org.apache.log4j.Logger

import scala.io.Source


class SparkHBaseApp2(@transient val ss: SparkSession, val principal: String, val keytab: String) extends Serializable {
  @transient
  val logger2 = Logger.getLogger(this.getClass)

  def run(): Unit = {
//    val bookdf = ss.table("default.books")
//    bookdf.show(false)
//    bookdf.repartition(2)

    import ss.implicits._
    val bookdf2 = Seq(("Chaika", "Chekhov"), ("We", "Zamyatin")).toDF("title", "author")
    bookdf2.show(false)
    bookdf2.repartition(3)

    bookdf2.foreachPartition((rowIt: Iterator[Row]) => {
      runPrivileged2(writeData(rowIt))
//      writeData2(rowIt)
    })
  }

  def runPrivileged2(func: Connection => Unit): Unit = {
    val logger2 = Logger.getLogger(this.getClass)
    logger2.info(s"Principal: $principal")
    logger2.info(s"Keytab: $keytab")

    val hbaseConf = HBaseConfiguration.create(new Configuration())
    var hbaseConn: Connection = null

    try {
      val ugi = UserGroupInformation.loginUserFromKeytabAndReturnUGI(principal, keytab)
      logger2.info("Logged as: " + ugi.toString)

      ugi.doAs(new PrivilegedExceptionAction[Unit] {
        override def run(): Unit = {
          hbaseConn = ConnectionFactory.createConnection(hbaseConf)
          val ss: SparkSession = SparkSession.getDefaultSession.orNull

          val auth = hbaseConn.getAdmin.toString()
          if (ss != null && TaskContext.get == null) {
            logger2.info("Use driver connection")
          } else if (ss != null && TaskContext.get != null) {
            logger2.info("Use executor connection")
          }
          logger2.info(s"Connection: ${hbaseConn.toString}")
          func(hbaseConn)
        }
      })

      ugi.logoutUserFromKeytab()
    }
    catch {
      case t: Exception =>
        logger2.error(s"Couldn't request the HBase authentication token: ${t}")
    }
    finally {
      if (hbaseConn != null) hbaseConn.close()
    }

  }

  def writeData2(info: Iterator[Row]) = {
    val mLogger = Logger.getLogger(this.getClass)

    info.foreach(r => mLogger.info(s"title: ${r.getString(0)}, author: ${r.getString(1)}"))

    mLogger.info(s"Principal: $principal")

    val fileContents = Source.fromFile(keytab).getLines.mkString
    mLogger.info(s"File content: ${fileContents.mkString}")

    mLogger.info(s"Keytab: $keytab")

  }

  def writeData(info: Iterator[Row] = Seq(Row("We", "Zamyatin")).toIterator)(conn: Connection): Unit = {

    val mLogger = Logger.getLogger(this.getClass)
    val table = conn.getTable(TableName.valueOf("book"))

    info.foreach((r: Row) => {
      val table = conn.getTable(TableName.valueOf("book"))
      val put = new Put(Bytes.toBytes(r.getString(0)))
      put.addColumn(Bytes.toBytes("info"), Bytes.toBytes("author"), Bytes.toBytes(r.getString(1)))
      table.put(put)
      table.close()
      mLogger.info(s"Writing to '${table.getName.getNameAsString}' row '${r.getString(0)}' values '${r.getString(1)}' ")
    })

    table.close();
  }
}

object SparkHBaseApp2 {

  private def fillTestTable(spark: SparkSession):Unit = {
    if (!spark.catalog.tableExists("default.books")) {
      spark.sql("create table if not exists default.books(title string, author string)")
      spark.sql("insert into table default.books values ('Chaika', 'Chekhov'), ('We', 'Zamyatin')")
      spark.table("default.books").show(false)
    }
  }

  private def fillTestDF(spark: SparkSession): DataFrame = {
    import spark.implicits._
    val bookdf2 = Seq(("Chaika", "Chekhov"), ("We", "Zamyatin")).toDF("title", "author")
    bookdf2.show(false)
    bookdf2.repartition(3)
  }

  private val KERBEROS_PRINCIPAL_CONF = "spark.yarn.principal"
  private val KERBEROS_KEYTAB_CONF = "spark.yarn.keytab"

  private var sparkConf: SparkConf = _
  @transient
  val logger: Logger = Logger.getLogger(this.getClass)

  @transient var ss: SparkSession = _

  def main(args: Array[String]): Unit = {
    ss = SparkSession
      .builder()
      .appName("Spark HBase app2")
//      .master("local[*]")
      .enableHiveSupport()
      .getOrCreate()

    val spark = ss
    if (args.length<1) {
      logger.error(s"The only argument - keytab filename is needed")
      System.exit(1)
    }

    val keytabFile = args(0)
    sparkConf = ss.sparkContext.getConf
    val sparkPrincipal = sparkConf.getOption(KERBEROS_KEYTAB_CONF).getOrElse("")
    val principal = sparkConf.getOption(KERBEROS_PRINCIPAL_CONF).getOrElse("")

//    spark.sparkContext.addFile(keytabFile)
    // Prepare test tables if not exists
    logger.info(s"Spark.yarn.principal principal: $principal")
    logger.info(s"Spark.yarn.principal principal: $principal")
    logger.info(s"Using principal: $principal")
    logger.info(s"Using keytab: $keytabFile")

//    logger.warn("HBase app started")
    sparkConf = spark.sparkContext.getConf

    // Check HBase configuration
    /*val HConfiguration: Configuration = HBaseConfiguration.create(new Configuration())
    logger.info("HBase configuration:\n " + HConfiguration)
    logger.info(s"znode: ${HConfiguration.get("zookeeper.znode.parent")}")*/

    val runner = new SparkHBaseApp2(
      spark,
      principal,
      keytabFile
    )
    runner.run()
  }
}


