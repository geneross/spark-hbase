package rosetta.spark.basic

import java.nio.file.Paths

import com.typesafe.scalalogging.StrictLogging
import org.apache.spark.sql.SparkSession

object SparkApp extends StrictLogging {

  def main(args: Array[String]): Unit  = {
      val ss = createSession("Example")
  }

  def createSession(configName: String): SparkSession = {
    SparkSession
      .builder()
      .appName(configName)
      .config("hive.exec.dynamic.partition", "true")
      .config("hive.exec.dynamic.partition.mode", "nonstrict")
      .config("javax.jdo.option.ConnectionURL", s"jdbc:derby:memory:metastore_db_$configName;create=true")
      .config("spark.default.parallelism", "1")
      .config("spark.sql.shuffle.partitions", "1")
      .config("spark.sql.warehouse.dir", s"${Paths.get("").toAbsolutePath.toString}/spark-warehouse_$configName")
      .enableHiveSupport()
      .master("local[1]")
      .getOrCreate()
  }

}
