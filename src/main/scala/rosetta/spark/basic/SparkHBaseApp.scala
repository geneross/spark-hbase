package rosetta.spark.basic

import java.io.StringWriter
import java.security.PrivilegedExceptionAction

import org.apache.hadoop.hbase.{CellUtil, KeyValue}
import org.apache.hadoop.hbase.client.{Put, Table}
import org.apache.spark.TaskContext
import org.apache.spark.sql.Row
import rosetta.spark.basic.SparkHBaseApp.{KERBEROS_KEYTAB_CONF, KERBEROS_PRINCIPAL_CONF, logger, sparkConf}

//import com.typesafe.scalalogging.StrictLogging
import org.apache.hadoop.conf.Configuration

import org.apache.hadoop.hbase.HBaseConfiguration
import org.apache.hadoop.hbase.TableName
import org.apache.hadoop.hbase.client.{Connection, ConnectionFactory, Get, Result}
import org.apache.hadoop.hbase.security.token.TokenUtil
import org.apache.hadoop.hbase.util.Bytes

import org.apache.hadoop.security.UserGroupInformation
import org.apache.hadoop.security.token.{Token, TokenIdentifier}
import org.apache.log4j.{Logger,Level}

import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession


class SparkHBaseApp(@transient val ss: SparkSession, val principal: String, val keytab: String) extends Serializable {

  @transient
  val logger2 =  Logger.getLogger(this.getClass)

  def run(): Unit = {

    val bookdf = ss.table("default.books")
    bookdf.show(false)
    logger2.info("LOGGER 2 IS Working!!!")
    logger2.info(s"PARTITIONS  BEFORE ${bookdf.rdd.getNumPartitions}")
    bookdf.repartition(3)
    logger2.info(s"PARTITIONS  AFTER ${bookdf.rdd.getNumPartitions}")

    bookdf.foreachPartition((rowIt: Iterator[Row]) =>
    {
      val log = Logger.getLogger(this.getClass)
      log.info(s"\n Number of rows in partition: ${rowIt.length}\n")
      log.info(s"\n Number of rows in partition: ${rowIt.mkString(",")}\n")
      println(s"\n Number of rows in partition: ${rowIt.length}\n")
      println(s"\n Number of rows in partition: ${rowIt.mkString(",")}\n")
      rowIt.foreach(r => log.info("\n" + r.getString(0) + r.getString(1) + "\n"))
      rowIt.foreach(r => println("\n" + r.getString(0) + r.getString(1) + "\n"))
      rowIt.foreach(r => log.info("\n" + r.toString() + "\n"))
      rowIt.foreach(r => println("\n" + r.toString() + "\n"))
      runPrivileged2(writeData(rowIt))
    })
  }

  def runPrivileged2(func: Connection => Unit): Unit = {
//    val principal: String = ss.sparkContext.getConf.get(KERBEROS_PRINCIPAL_CONF)
//    val keytab:String = ss.sparkContext.getConf.get(KERBEROS_KEYTAB_CONF)

    val logger2 = Logger.getLogger(this.getClass)
    logger2.info(s"Principal: $principal")
    logger2.info(s"Keytab: $keytab")

    val hbaseConf = HBaseConfiguration.create(new Configuration())
    var hbaseConn: Connection = null

    try {
      val ugi = UserGroupInformation.loginUserFromKeytabAndReturnUGI(principal, keytab)
      logger2.info("Logged as: " + ugi.toString)

      ugi.doAs(new PrivilegedExceptionAction[Unit] {
        override def run(): Unit = {
          hbaseConn = ConnectionFactory.createConnection(hbaseConf)
          val ss: SparkSession = SparkSession.getDefaultSession.orNull

          val auth = hbaseConn.getAdmin.toString()
          if (ss != null && TaskContext.get == null) {
            logger2.info("Use driver connection")
          } else if (ss != null && TaskContext.get != null){
            logger2.info("Use executor connection")
          }
          logger2.info(s"Connection: ${hbaseConn.toString}")
          func(hbaseConn)
        }
      })

      ugi.logoutUserFromKeytab()
    }
    catch {
      case t: Exception =>
        logger2.error(s"Couldn't request the HBase authentication token: ${t}")
    }
    finally {
      if(hbaseConn != null) hbaseConn.close()
    }

  }

  def writeData(info: Iterator[Row] = Seq(Row("We", "Zamyatin")).toIterator)(conn: Connection): Unit = {

    val mLogger = Logger.getLogger(this.getClass)
    val table = conn.getTable(TableName.valueOf("book"))
    mLogger.info(s"Row iterator = ${info.mkString(",")}")
    mLogger.info(s"Row iterator length = ${info.length}")
    println(s"Row iterator length = ${info.length}")
//    val put = new Put(Bytes.toBytes("We"))
//    put.addColumn(Bytes.toBytes("info"), Bytes.toBytes("author"), Bytes.toBytes("Zamyatin"))
//    table.put(put);


    //    val cfAndCn = CellUtil.parseColumn(Bytes.toBytes("info:author"))

    info.foreach((r: Row) => {
      val table = conn.getTable(TableName.valueOf("book"))
      val put = new Put(Bytes.toBytes(r.getString(0)))
      put.addColumn(Bytes.toBytes("info"), Bytes.toBytes("author"), Bytes.toBytes(r.getString(1)))
      mLogger.info(s"Writing to '${table.getName.getNameAsString}' row '${r.getString(0)}' values '${r.getString(1)}' ")
      println(s"Writing to '${table.getName.getNameAsString}' row '${r.getString(0)}' values '${r.getString(1)}' ")
      table.put(put)
      table.close()
    })

    //    table.put(put);


    table.close();
  }
}


object SparkHBaseApp /*extends StrictLogging*/ {

  private val KERBEROS_PRINCIPAL_CONF = "spark.yarn.principal"
  private val KERBEROS_KEYTAB_CONF = "spark.yarn.keytab"

  private var sparkConf: SparkConf = _
  @transient
  val logger =  Logger.getLogger(this.getClass)

  @transient var ss: SparkSession  = _

  def main(args: Array[String]): Unit  = {
    val spark = SparkSession
      .builder()
      .appName("Spark HBase app")
      .enableHiveSupport()
//      .master("local[*]")
      .getOrCreate()

//    val logs =  Logger.getLogger(this.getClass)
//    val logger =  Logger.getLogger(this.getClass)

    logger.warn("HBase app started")
    sparkConf = spark.sparkContext.getConf

    val ss: SparkSession = SparkSession.getDefaultSession.getOrElse(throw new IllegalStateException())
    ss.sparkContext.getConf
    val logs =  Logger.getLogger(this.getClass)

    logs.setLevel(Level.DEBUG)
    logs.info("INFO")
    logs.debug("Debug")

//    val config: Config = ArgParser.parseArgs(args)
//    logger.info("Utility config:\n " + config)

//    val HConfiguration: Configuration = HConfigurationHelper.create(config.getHbaseTarget)
    val HConfiguration: Configuration = HBaseConfiguration.create(new Configuration())
    logger.info("HBase configuration:\n " + HConfiguration)

    logger.info(s"znode: ${HConfiguration.get("zookeeper.znode.parent")}")

    val writer = new StringWriter()
    Configuration.dumpConfiguration(HConfiguration, writer)
    logger.info(s"Conf: ${writer.toString}")

    spark.sql("show databases").show(false)

//    logger.info(s"Auth Config: ${config.getAuthentication.toString}")
//    logger.info(s"HbaseTarget: ${config.getHbaseTarget.toString}")
//    logger.info(s"znode: ${HConfiguration.get}")

//    SparkHBaseConf

    /*val config: Config = ArgParser.parseArgs(args)
        ConfigValidator.prepareAndValidate(config)
        log.info("Utility config:\n " + config)

        val HConfiguration: Configuration = HConfigurationHelper.create(config.getHbaseTarget)
        log.info("HBase configuration:\n " + HConfiguration)
        val ss: SparkSession = Spark.get.getSession
        ss.sql("drop table if exists default.salaries")
        ss.sql("create external table if not exists default.salaries(salary string) location '/tmp/default'")
        ss.sql("insert into table default.salaries values ('1'), ('2')")
        ss.table("default.salaries").show()

        recreateHTableIfNeeded(config, HConfiguration)

        val writer: Writer = Writers.getWriter(config, HConfiguration)
        writer.write()*/
    logger.info(s"znode parent: ${HConfiguration.get("zookeeper.znode.parent")}")
    logger.info(s"hbase quorum: ${HConfiguration.get("hbase.zookeeper.quorum")}")
    logger.info(s"hbase master principal: ${HConfiguration.get("hbase.master.kerberos.principal")}")
    logger.info(s"hbase region principal: ${HConfiguration.get("hbase.regionserver.kerberos.principal")}")

    obtainHBaseToken()

    runPrivileged(readData("Idiot"))
//    runPrivileged(writeData())
//    runPrivileged(readData("We"))

    val conf = HBaseConfiguration.create
    val connection = ConnectionFactory.createConnection(conf)
    logger.info(s"Driver connection: ${connection.getAdmin.toString}")
/*    val tableName = TableName.valueOf("book")

    val table: Table = connection.getTable(tableName)

    logger.info(s"Getting table ${table.getName}")

    val get: Get = new Get(Bytes.toBytes("Idiot"))
    get.addColumn(Bytes.toBytes("info"), Bytes.toBytes("author"))
    val result: Result = table.get(get)
    val `val` = result.getValue(Bytes.toBytes("info"), Bytes.toBytes("author"))
    System.out.println("Value: " + Bytes.toString(`val`))
    logger.info(s"Value = ${Bytes.toString(`val`)}")
    table.close()*/

/*    val bookdf = spark.table("default.books")
    bookdf.show(false)
    logger.info(s"PARTITIONS  BEFORE ${bookdf.rdd.getNumPartitions}")
    bookdf.repartition(3)
    logger.info(s"PARTITIONS  AFTER ${bookdf.rdd.getNumPartitions}")*/

    val app = new SparkHBaseApp(spark, sparkConf.get(KERBEROS_PRINCIPAL_CONF), sparkConf.get(KERBEROS_KEYTAB_CONF))
    app.run()

    connection.close()

    spark.stop()
  }


  def readData(row: String = "Idiot")(conn: Connection):Unit = {
    val tableName = TableName.valueOf("book")

    val table: Table = conn.getTable(tableName)

    logger.info(s"Getting table ${table.getName}")

    val get: Get = new Get(Bytes.toBytes(row))
    get.addColumn(Bytes.toBytes("info"), Bytes.toBytes("author"))
    val result: Result = table.get(get)
    val `val` = result.getValue(Bytes.toBytes("info"), Bytes.toBytes("author"))
    System.out.println("Value: " + Bytes.toString(`val`))
    logger.info(s">>>>>>>>>>>>>>>>>>Value = ${Bytes.toString(`val`)}")
    table.close()
  }


  def writeData(info: Iterator[Row] = Seq(Row("We", "Zamyatin")).toIterator)(conn: Connection): Unit = {

    val table = conn.getTable(TableName.valueOf("book"))
    val put = new Put(Bytes.toBytes("We"))
    put.addColumn(Bytes.toBytes("info"), Bytes.toBytes("author"), Bytes.toBytes("Zamyatin"))
    table.put(put);


//    val cfAndCn = CellUtil.parseColumn(Bytes.toBytes("info:author"))

    info.foreach((r: Row) => {
      val put = new Put(Bytes.toBytes(r.getString(0)))
      put.addColumn(Bytes.toBytes("info"), Bytes.toBytes("author"), Bytes.toBytes(r.getString(1)))
      table.put(put)

    })

//    table.put(put);


    table.close();

  /*

        final byte[] cfBytes = Bytes.toBytes(config.getHbaseTarget().getTable().getColumnFamily().getName());
        final byte[] qBytes = Bytes.toBytes(config.getHbaseTarget().getTable().getColumnFamily().getColumnName());
        final int batchSize = config.getLoad().getSimple().getBatchSize();

        log.info(String.format("Col  family: %s", Bytes.toString(cfBytes)));
        log.info(String.format("Col qualify: %s", Bytes.toString(qBytes)));
        log.info(String.format("Batch  size: %s", Bytes.toString(qBytes)));

        final AuthenticationConfig authConf = config.getAuthentication();
        log.info(String.format("Auth type: %s", authConf.getType()));


        dataset.toJavaRDD().foreachPartition(new VoidFunction<Iterator<Row>>() {
            @Override
            public void call(Iterator<Row> rowIterator) {
                // connect to hbase and get table
                final Configuration HBaseConfiguration = getHBaseConfiguration();

                Connection connection;
                final Table table;


                try {

                    // todo: здесь должна быть логика определения конфигурации.

                    final SparkConf sparkConf = Spark.get().getSession().sparkContext().getConf();
                    final String principal = sparkConf.get(KERBEROS_PRINCIPAL_CONF);
                    final String keytab = sparkConf.get(KERBEROS_KEYTAB_CONF);

                    log.info("Principal: " + principal);
                    log.info("Keytab: " + keytab);

                    final UserGroupInformation ugi = UserGroupInformation.loginUserFromKeytabAndReturnUGI(principal, keytab);
                    log.info("Logged as: " + ugi.toString());

                    ugi.doAs(new PrivilegedExceptionAction<Void>() {
                        public Void run() throws IOException {
//                            Connection conn = ConnectionFactory.createConnection(HBaseConfiguration);
                            final Connection conn = getConnection(HBaseConfiguration);
                            final Table tbl = HBaseHelper.getHTable(conn, config.getHbaseTarget());

                            log.info("Success connect to hbase and get table");
                            log.info("Table: " + tbl.getName().getNameAsString());
                            log.info("Table namespace: " + tbl.getName().getNamespaceAsString());

                            final List<Put> puts = new ArrayList<>(batchSize);
                            while (rowIterator.hasNext()) {
                                final Row row = rowIterator.next();
                                log.info(String.format("Schema: %s", row.schema().toString()));
                                log.info(String.format("Row   : %s", row.toString()));
                                final byte[] keyBytes = Bytes.toBytes(keyExtractor.extract(row));
                                final byte[] valueBytes = rowJsonConverter.convert(row).getBytes();
                                puts.add(new Put(keyBytes).addColumn(cfBytes, qBytes, valueBytes));
                                if (puts.size() >= batchSize) flush(tbl, puts);
                            }

                            flush(tbl, puts);

                            closeConnectionSilently(conn);
                            return null;
                        }
                    });

                    connection = ugi.doAs((PrivilegedExceptionAction<Connection>) () ->
//                            ConnectionFactory.createConnection(HBaseConfiguration)
                            getConnection(HBaseConfiguration)
                    );

//                    connection = getConnection(HBaseConfiguration);
//                    connection = getConnection(HBaseConfiguration);
                    table = HBaseHelper.getHTable(connection, config.getHbaseTarget());
                    log.info("Success connect to hbase and get table");
                    log.info("Table: " + table.getName().getNameAsString());
                    log.info("Table namespace: " + table.getName().getNamespaceAsString());

/*                    // put rows to hbase in batches
                    final List<Put> puts = new ArrayList<>(batchSize);
                    while (rowIterator.hasNext()) {
                        final Row row = rowIterator.next();
                        final byte[] keyBytes = Bytes.toBytes(keyExtractor.extract(row));
                        final byte[] valueBytes = rowJsonConverter.convert(row).getBytes();
                        puts.add(new Put(keyBytes).addColumn(cfBytes, qBytes, valueBytes));
                        if (puts.size() >= batchSize) flush(table, puts);
                    }

                    flush(table, puts);*/
                } catch (IOException | InterruptedException e) {
                    log.error("Error when connect to hbase and get table");
                    throw new WriterException(e);
                }

/*                // put rows to hbase in batches
                final List<Put> puts = new ArrayList<>(batchSize);
                while (rowIterator.hasNext()) {
                    final Row row = rowIterator.next();
                    final byte[] keyBytes = Bytes.toBytes(keyExtractor.extract(row));
                    final byte[] valueBytes = rowJsonConverter.convert(row).getBytes();
                    puts.add(new Put(keyBytes).addColumn(cfBytes, qBytes, valueBytes));
                    if (puts.size() >= batchSize) flush(table, puts);
                }

                flush(table, puts);*/
//                closeConnectionSilently(connection);
            }
        });

        log.info("Finish SimpleSparkWriter.write(Dataset<Row>)");
    }
  * */
  }

  def runPrivileged(func: Connection => Unit): Unit = {
    val principal: String = sparkConf.get(KERBEROS_PRINCIPAL_CONF)
    val keytab:String = sparkConf.get(KERBEROS_KEYTAB_CONF)

    logger.info(s"Principal: $principal")
    logger.info(s"Keytab: $keytab")

    val hbaseConf = HBaseConfiguration.create(new Configuration())
    var hbaseConn: Connection = null

    try {
      val ugi = UserGroupInformation.loginUserFromKeytabAndReturnUGI(principal, keytab)
      logger.info("Logged as: " + ugi.toString)

      ugi.doAs(new PrivilegedExceptionAction[Unit] {
        override def run(): Unit = {
          hbaseConn = ConnectionFactory.createConnection(hbaseConf)
          val ss: SparkSession = SparkSession.getDefaultSession.orNull

          val auth = hbaseConn.getAdmin.toString()
          if (ss != null && TaskContext.get == null) {
            logger.info("Use driver connection")
          } else if (ss != null && TaskContext.get != null){
            logger.info("Use executor connection")
          }
          logger.info(s"Connection: ${hbaseConn.toString}")
          func(hbaseConn)
        }
      })

      ugi.logoutUserFromKeytab()
    }
    catch {
      case t: Exception =>
        logger.error(s"Couldn't request the HBase authentication token: ${t}")
    }
    finally {
      if(hbaseConn != null) hbaseConn.close()
    }

  }

  /**
   * Requests HBase new delegation / authentication token from the service
   */
//  def obtainHBaseToken(): Token[_ <: TokenIdentifier] = {
  def obtainHBaseToken(principal: String = sparkConf.get(KERBEROS_PRINCIPAL_CONF),
                       keytab:String = sparkConf.get(KERBEROS_KEYTAB_CONF)): Connection = {
//    val principal: String = sparkConf.get(KERBEROS_PRINCIPAL_CONF)
//    val keytab:String = sparkConf.get(KERBEROS_KEYTAB_CONF)

    logger.info(s"Principal: $principal")
    logger.info(s"Keytab: $keytab")

    val hbaseConf = HBaseConfiguration.create(new Configuration())
//    val hbaseConf2 = HConfigurationHelper.create(config.getHbaseTarget)
    var hbaseConn: Connection = null
    var hbaseToken: Token[_ <: TokenIdentifier] = null

    try {
      val ugi = UserGroupInformation.loginUserFromKeytabAndReturnUGI(principal, keytab)
      logger.info("Logged as: " + ugi.toString)


      ugi.doAs(new PrivilegedExceptionAction[Unit] {
        override def run(): Unit = {
          hbaseConn = ConnectionFactory.createConnection(hbaseConf)
          readData()(ConnectionFactory.createConnection(hbaseConf))
        }
      })

/*      hbaseToken = ugi.doAs(new PrivilegedExceptionAction[Token[_ <: TokenIdentifier]] {
        override def run(): Token[_ <: TokenIdentifier] = {
          hbaseConn = ConnectionFactory.createConnection(hbaseConf)
          TokenUtil.obtainToken(hbaseConn)
        }
      })
      ugi.doAs(new PrivilegedExceptionAction[Connection] {
        override def run(): Connection = {
          ConnectionFactory.createConnection(hbaseConf)
        }
      })*/
      ugi.logoutUserFromKeytab()
    }
    catch {
      case t: Exception =>
        logger.error(s"Couldn't request the HBase authentication token: ${t}")
    }
    finally {
      if(hbaseConn != null) hbaseConn.close()
    }

//    hbaseToken
    hbaseConn
  }
}
