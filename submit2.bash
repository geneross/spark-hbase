export BLD_LIBS_DIR=./build/tmp-dist/lib/
export SUBMIT_LIBS=./lib2/

echo ${BLD_LIBS_DIR}
mkdir -p ${SUBMIT_LIBS}

cp ${BLD_LIBS_DIR}/hbase-client-2.2*.jar ${SUBMIT_LIBS}
cp ${BLD_LIBS_DIR}/hbase-common-2.2*.jar ${SUBMIT_LIBS}
cp ${BLD_LIBS_DIR}/hbase-shaded-miscellaneous-2.2*.jar ${SUBMIT_LIBS}
cp ${BLD_LIBS_DIR}/hbase-protocol-shaded-2.2*.jar ${SUBMIT_LIBS}
cp ${BLD_LIBS_DIR}/hbase-shaded-protobuf-2.1.0.jar ${SUBMIT_LIBS}
cp ${BLD_LIBS_DIR}/hbase-shaded-netty-2.1.0.jar ${SUBMIT_LIBS}
cp ${BLD_LIBS_DIR}/hbase-protocol-2.2*.jar ${SUBMIT_LIBS}

spark-submit --verbose \
 --files /etc/hbase/conf/hbase-site.xml \
 --conf spark.driver.extraClassPath=/etc/hbase/conf \
 --conf spark.executor.extraClassPath=/etc/hbase/conf \
 --conf spark.yarn.principal=${PRINCIPAL} \
 --conf spark.yarn.keytab=${KEYTAB} \
 --class rosetta.spark.basic.SparkHBaseApp \
 --jars $(echo ${SUBMIT_LIBS}/*.jar | tr ' ' ',') \
 --deploy-mode client \
 --master yarn \
 --name Spark-HBase-App\
 build/libs/spark-hbase-1.0.jar
