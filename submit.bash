spark-submit --verbose \
 --class rosetta.spark.basic.SparkHbaseApp \
 --jars $(echo ./build/tmp-dist/lib/*.jar | tr ' ' ',') \
 --name Spark-HBase-App\
 build/libs/spark-hbase-1.0.jar